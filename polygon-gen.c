#include "stdio.h"
#include "stdlib.h"
#include "math.h"

#define PI 3.1415926

int main(int argc, char * argv[]) {
	int points = 5;
	int radius = 1;

	switch (argc) {
	case 3:
		radius = abs(atoi(argv[2]));
	case 2:
		points = abs(atoi(argv[1]));
	case 1:
		break;
	default:
		return 1;
	}
	
	double x;
	double y;
	scanf("%lf %lf", &x, &y);

	if(points < 3) {
		printf("Not a regular polygon!\n");
	}
	else {
		double sectAngle = 2 * PI / points;

		for(int i = 0; i < points; i++) {
			double angle = i * sectAngle;
			printf("%lf, %lf\n", x + radius * cos(angle), y + radius * sin(angle));
		}
	}

	return 0;
}
